# translation of powerdevilglobalconfig.po to Gujarati
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Sweta Kothari <swkothar@redhat.com>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: powerdevilglobalconfig\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-28 02:37+0000\n"
"PO-Revision-Date: 2010-12-21 17:01+0530\n"
"Last-Translator: Sweta Kothari <swkothar@redhat.com>\n"
"Language-Team: Gujarati\n"
"Language: gu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "શ્ર્વેતા કોઠારી, ઉમંગ ભટ્ટ"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "swkothar@redhat.com, bhatt.umang7@gmail.com"

#: GeneralPage.cpp:240
#, kde-format
msgid "The Power Management Service appears not to be running."
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, batteryLevelsLabel)
#: generalPage.ui:22
#, fuzzy, kde-format
#| msgid "Battery Levels"
msgid "<b>Battery Levels                     </b>"
msgstr "બેટરી સ્તર"

#. i18n: ectx: property (text), widget (QLabel, lowLabel)
#: generalPage.ui:29
#, fuzzy, kde-format
#| msgid "Low battery level"
msgid "&Low level:"
msgstr "નીચુ બેટરી સ્તર"

#. i18n: ectx: property (toolTip), widget (QSpinBox, lowSpin)
#: generalPage.ui:39
#, kde-format
msgid "Low battery level"
msgstr "નીચુ બેટરી સ્તર"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, lowSpin)
#: generalPage.ui:42
#, kde-format
msgid "Battery will be considered low when it reaches this level"
msgstr ""

#. i18n: ectx: property (suffix), widget (QSpinBox, lowSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, criticalSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, lowPeripheralSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, chargeStartThresholdSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, chargeStopThresholdSpin)
#: generalPage.ui:45 generalPage.ui:71 generalPage.ui:114 generalPage.ui:167
#: generalPage.ui:230
#, no-c-format, kde-format
msgid "%"
msgstr "%"

#. i18n: ectx: property (text), widget (QLabel, criticalLabel)
#: generalPage.ui:55
#, fuzzy, kde-format
#| msgid "When battery is at low level"
msgid "&Critical level:"
msgstr "જ્યારે બેટરી નીચા સ્તર પર છે"

#. i18n: ectx: property (toolTip), widget (QSpinBox, criticalSpin)
#: generalPage.ui:65
#, kde-format
msgid "Critical battery level"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QSpinBox, criticalSpin)
#: generalPage.ui:68
#, kde-format
msgid "Battery will be considered critical when it reaches this level"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, BatteryCriticalLabel)
#: generalPage.ui:81
#, fuzzy, kde-format
#| msgid "When battery is at low level"
msgid "A&t critical level:"
msgstr "જ્યારે બેટરી નીચા સ્તર પર છે"

#. i18n: ectx: property (text), widget (QLabel, lowPeripheralLabel)
#: generalPage.ui:107
#, kde-format
msgid "Low level for peripheral devices:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, batteryThresholdLabel)
#: generalPage.ui:130
#, kde-format
msgid "Charge Limit"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, batteryThresholdExplanation)
#: generalPage.ui:137
#, no-c-format, kde-format
msgid ""
"<html><head/><body><p>Keeping the battery charged 100% over a prolonged "
"period of time may accelerate deterioration of battery health. By limiting "
"the maximum battery charge you can help extend the battery lifespan.</p></"
"body></html>"
msgstr ""

#. i18n: ectx: property (text), widget (KMessageWidget, chargeStopThresholdMessage)
#: generalPage.ui:147
#, kde-format
msgid ""
"You might have to disconnect and re-connect the power source to start "
"charging the battery again."
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, chargeStartThresholdLabel)
#: generalPage.ui:157
#, kde-format
msgid "Start charging only once below:"
msgstr ""

#. i18n: ectx: property (specialValueText), widget (QSpinBox, chargeStartThresholdSpin)
#: generalPage.ui:164
#, kde-format
msgid "Always charge when plugged in"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, pausePlayersLabel)
#: generalPage.ui:177
#, kde-format
msgid "Pause media players when suspending:"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, pausePlayersCheckBox)
#: generalPage.ui:184
#, kde-format
msgid "Enabled"
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, notificationsButton)
#: generalPage.ui:203
#, kde-format
msgid "Configure Notifications…"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: generalPage.ui:216
#, fuzzy, kde-format
#| msgid "Advanced Battery Settings"
msgid "Other Settings"
msgstr "ઉન્નત બેટરી સુયોજનો"

#. i18n: ectx: property (text), widget (QLabel, chargeStopThresholdLabel)
#: generalPage.ui:223
#, kde-format
msgid "Stop charging at:"
msgstr ""

#~ msgid "Do nothing"
#~ msgstr "કંઇ જ કરો નહિં"

#, fuzzy
#~| msgid "Sleep"
#~ msgctxt "Suspend to RAM"
#~ msgid "Sleep"
#~ msgstr "સ્લીપ"

#~ msgid "Hibernate"
#~ msgstr "હાઇબરનેટ"

#, fuzzy
#~| msgid "Shutdown"
#~ msgid "Shut down"
#~ msgstr "બંધ કરો"

#~ msgid "<b>Events</b>"
#~ msgstr "<b>ઘટનાઓ</b>"

#~ msgid "Battery is at low level at"
#~ msgstr "બેટરી નીચા સ્તર પર છે"

#~ msgid "Global Power Management Configuration"
#~ msgstr "વૈશ્ર્વિક પાવર સંચાલન રૂપરેખાંકન"

#~ msgid ""
#~ "A global power management configurator for KDE Power Management System"
#~ msgstr "KDE પાવર સંચાલન સિસ્ટમ માટે વૈશ્ર્વિક પાવર સંચાલન રૂપરેખાંકર"

#~ msgid "(c), 2010 Dario Freddi"
#~ msgstr "(c), 2010 Dario Freddi"

#~ msgid "Dario Freddi"
#~ msgstr "Dario Freddi"

#~ msgid "Maintainer"
#~ msgstr "મેન્ટેનર"

#~ msgid "Settings and Profile"
#~ msgstr "સુયોજનો અને રૂપરેખાંકન"

#~ msgid ""
#~ "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
#~ "REC-html40/strict.dtd\">\n"
#~ "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/"
#~ "css\">\n"
#~ "p, li { white-space: pre-wrap; }\n"
#~ "</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; "
#~ "font-weight:400; font-style:normal;\">\n"
#~ "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
#~ "right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
#~ "weight:600;\">Profile Assignment</span></p></body></html>"
#~ msgstr ""
#~ "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
#~ "REC-html40/strict.dtd\">\n"
#~ "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/"
#~ "css\">\n"
#~ "p, li { white-space: pre-wrap; }\n"
#~ "</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; "
#~ "font-weight:400; font-style:normal;\">\n"
#~ "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
#~ "right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
#~ "weight:600;\">Profile Assignment</span></p></body></html>"

#~ msgid "When battery is at warning level"
#~ msgstr "જ્યારે બેટરી ચેતવણી સ્તર પર છે"

#~ msgid "Battery is at warning level at"
#~ msgstr "બેટરી ચેતવણી સ્તર પર છે"

#~ msgid "Warning battery level"
#~ msgstr "ચેતવણી બેટરી સ્તર"
