# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Antonis Geralis <capoiosct@gmail.com>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-28 02:37+0000\n"
"PO-Revision-Date: 2013-11-25 14:52+0200\n"
"Last-Translator: Antonis Geralis <capoiosct@gmail.com>\n"
"Language-Team: Greek <kde-i18n-doc@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.5\n"

#: ErrorOverlay.cpp:27
#, kde-format
msgid ""
"Power Management configuration module could not be loaded.\n"
"%1"
msgstr ""
"Η φόρτωση του αρθρώματος διαχείρισης ενέργειας ήταν αδύνατη.\n"
"%1"

#: PowerButtonActionModel.cpp:31
#, kde-format
msgid "Do nothing"
msgstr ""

#: PowerButtonActionModel.cpp:40
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr ""

#: PowerButtonActionModel.cpp:50
#, kde-format
msgid "Hibernate"
msgstr ""

#: PowerButtonActionModel.cpp:60
#, kde-format
msgid "Hybrid sleep"
msgstr ""

#: PowerButtonActionModel.cpp:69
#, kde-format
msgctxt "Power down the computer"
msgid "Shut down"
msgstr ""

#: PowerButtonActionModel.cpp:77
#, kde-format
msgid "Lock screen"
msgstr ""

#: PowerButtonActionModel.cpp:85
#, kde-format
msgid "Prompt log out dialog"
msgstr ""

#: PowerButtonActionModel.cpp:93
#, kde-format
msgid "Turn off screen"
msgstr ""

#: PowerButtonActionModel.cpp:101
#, kde-format
msgid "Toggle screen on/off"
msgstr ""
