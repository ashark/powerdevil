# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Antonis Geralis <capoiosct@gmail.com>, 2011, 2012.
# Dimitrios Glentadakis <dglent@gmail.com>, 2012.
# Dimitris Kardarakos <dimkard@gmail.com>, 2014, 2015, 2016, 2017.
# Stelios <sstavra@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: powerdevilglobalconfig\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-28 02:37+0000\n"
"PO-Revision-Date: 2021-07-25 10:10+0300\n"
"Last-Translator: Stelios <sstavra@gmail.com>\n"
"Language-Team: Greek <kde-i18n-el@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.04.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Αντώνης Γέραλης"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "capoiosct@gmail.com"

#: GeneralPage.cpp:240
#, kde-format
msgid "The Power Management Service appears not to be running."
msgstr "Η υπηρεσία διαχείρισης ισχύος μάλλον δεν εκτελείται."

#. i18n: ectx: property (text), widget (QLabel, batteryLevelsLabel)
#: generalPage.ui:22
#, kde-format
msgid "<b>Battery Levels                     </b>"
msgstr "<b>Επίπεδα μπαταρίας                     </b>"

#. i18n: ectx: property (text), widget (QLabel, lowLabel)
#: generalPage.ui:29
#, kde-format
msgid "&Low level:"
msgstr "&Χαμηλό επίπεδο:"

#. i18n: ectx: property (toolTip), widget (QSpinBox, lowSpin)
#: generalPage.ui:39
#, kde-format
msgid "Low battery level"
msgstr "Χαμηλό επίπεδο μπαταρίας"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, lowSpin)
#: generalPage.ui:42
#, kde-format
msgid "Battery will be considered low when it reaches this level"
msgstr "Η μπαταρία θα θεωρείται χαμηλή όταν φτάσει αυτό το επίπεδο"

#. i18n: ectx: property (suffix), widget (QSpinBox, lowSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, criticalSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, lowPeripheralSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, chargeStartThresholdSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, chargeStopThresholdSpin)
#: generalPage.ui:45 generalPage.ui:71 generalPage.ui:114 generalPage.ui:167
#: generalPage.ui:230
#, no-c-format, kde-format
msgid "%"
msgstr "%"

#. i18n: ectx: property (text), widget (QLabel, criticalLabel)
#: generalPage.ui:55
#, kde-format
msgid "&Critical level:"
msgstr "&Κρίσιμο επίπεδο:"

#. i18n: ectx: property (toolTip), widget (QSpinBox, criticalSpin)
#: generalPage.ui:65
#, kde-format
msgid "Critical battery level"
msgstr "Κρίσιμο επίπεδο μπαταρίας"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, criticalSpin)
#: generalPage.ui:68
#, kde-format
msgid "Battery will be considered critical when it reaches this level"
msgstr "Η μπαταρία θα θεωρείται κρίσιμη όταν φτάσει αυτό το επίπεδο"

#. i18n: ectx: property (text), widget (QLabel, BatteryCriticalLabel)
#: generalPage.ui:81
#, kde-format
msgid "A&t critical level:"
msgstr "Σ&ε κρίσιμο επίπεδο:"

#. i18n: ectx: property (text), widget (QLabel, lowPeripheralLabel)
#: generalPage.ui:107
#, kde-format
msgid "Low level for peripheral devices:"
msgstr "Χαμηλό επίπεδο για περιφερειακές συσκευές:"

#. i18n: ectx: property (text), widget (QLabel, batteryThresholdLabel)
#: generalPage.ui:130
#, kde-format
msgid "Charge Limit"
msgstr "Όριο φόρτισης"

#. i18n: ectx: property (text), widget (QLabel, batteryThresholdExplanation)
#: generalPage.ui:137
#, no-c-format, kde-format
msgid ""
"<html><head/><body><p>Keeping the battery charged 100% over a prolonged "
"period of time may accelerate deterioration of battery health. By limiting "
"the maximum battery charge you can help extend the battery lifespan.</p></"
"body></html>"
msgstr ""
"<html><head/><body><p>Διατηρώντας τη μπαταρία φορτισμένη 100% για μεγάλη "
"χρονική περίοδο ίσως επιδεινώσει την υγεία της μπαταρίας. Περιορίζοντας τη "
"μέγιστη φόρτιση της μπαταρίας βοηθάτε στην επιμήκυνση του χρόνου ζωής της "
"μπαταρίας.</p></body></html>"

#. i18n: ectx: property (text), widget (KMessageWidget, chargeStopThresholdMessage)
#: generalPage.ui:147
#, kde-format
msgid ""
"You might have to disconnect and re-connect the power source to start "
"charging the battery again."
msgstr ""
"Ίσως χρειαστεί να αποσυνδέσετε και να επανασυνδέσετε την πηγή ισχύος για να "
"ξεκινήσει πάλι η φόρτιση."

#. i18n: ectx: property (text), widget (QLabel, chargeStartThresholdLabel)
#: generalPage.ui:157
#, kde-format
msgid "Start charging only once below:"
msgstr "Έναρξη φόρτισης μία φορά κάτω από:"

#. i18n: ectx: property (specialValueText), widget (QSpinBox, chargeStartThresholdSpin)
#: generalPage.ui:164
#, kde-format
msgid "Always charge when plugged in"
msgstr "Να γίνεται απάντα φόρτιση όταν είναι σε σύνδεση"

#. i18n: ectx: property (text), widget (QLabel, pausePlayersLabel)
#: generalPage.ui:177
#, kde-format
msgid "Pause media players when suspending:"
msgstr "Παύση αναπαραγωγής πολυμέσων κατά την αναστολή:"

#. i18n: ectx: property (text), widget (QCheckBox, pausePlayersCheckBox)
#: generalPage.ui:184
#, kde-format
msgid "Enabled"
msgstr "Ενεργοποιημένο"

#. i18n: ectx: property (text), widget (QPushButton, notificationsButton)
#: generalPage.ui:203
#, kde-format
msgid "Configure Notifications…"
msgstr "Διαμόρφωση ειδοποιήσεων…"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: generalPage.ui:216
#, kde-format
msgid "Other Settings"
msgstr "Άλλες ρυθμίσεις"

#. i18n: ectx: property (text), widget (QLabel, chargeStopThresholdLabel)
#: generalPage.ui:223
#, kde-format
msgid "Stop charging at:"
msgstr "Τερματισμός φόρτισης στο:"

#~ msgid "Do nothing"
#~ msgstr "Καμία ενέργεια"

#~ msgctxt "Suspend to RAM"
#~ msgid "Sleep"
#~ msgstr "Κοίμηση"

#~ msgid "Hibernate"
#~ msgstr "Νάρκη"

#~ msgid "Shut down"
#~ msgstr "Τερματισμός"

#~ msgid ""
#~ "The Power Management Service appears not to be running.\n"
#~ "This can be solved by starting or scheduling it inside \"Startup and "
#~ "Shutdown\""
#~ msgstr ""
#~ "Η υπηρεσία διαχείρισης ενέργειας δεν φαίνεται να εκτελείται.\n"
#~ "Αυτό μπορεί να επιλυθεί με την έναρξη ή τον προγραμματισμό της από το "
#~ "\"Εκκίνηση και τερματισμός\""

#~ msgid "Suspend"
#~ msgstr "Αναστολή"

#~ msgid "<b>Events</b>"
#~ msgstr "<b>Γεγονότα</b>"

#~ msgid ""
#~ "When this option is selected, applications will not be allowed to inhibit "
#~ "sleep when the lid is closed"
#~ msgstr ""
#~ "Όταν επιλεχθεί η ρύθμιση αυτή, δεν θα επιτρέπεται στις εφαρμογές να "
#~ "αποτρέψουν την κοίμηση όταν το καπάκι είναι κλειστό"

#~ msgid "Never prevent an action on lid close"
#~ msgstr "Να μην εμποδιστεί ποτέ μια ενέργεια όταν το καπάκι κλείσει"

#~ msgid "Locks screen when waking up from suspension"
#~ msgstr "Κλείδωμα της οθόνης κατά την επαναφορά από την αναστολή"

#~ msgid "You will be asked for a password when resuming from sleep state"
#~ msgstr ""
#~ "Θα ερωτηθείτε για τον κωδικό πρόσβασης κατά την επαναφορά από την "
#~ "κατάσταση κοίμησης"

#~ msgid "Loc&k screen on resume"
#~ msgstr "Κλεί&δωμα οθόνης στην επαναφορά"

#~ msgid "Battery is at low level at"
#~ msgstr "Η μπαταρία είναι σε χαμηλό επίπεδο στο"

#~ msgid "When battery is at critical level"
#~ msgstr "Όταν η μπαταρία είναι σε κρίσιμο επίπεδο"

#~ msgid "Global Power Management Configuration"
#~ msgstr "Καθολική ρύθμιση διαχείρισης ενέργειας"

#~ msgid ""
#~ "A global power management configurator for KDE Power Management System"
#~ msgstr ""
#~ "Εργαλείο καθολικής ρύθμισης διαχείρισης ενέργειας για το σύστημα "
#~ "διαχείρισης ενέργειας του KDE"

#~ msgid "(c), 2010 Dario Freddi"
#~ msgstr "(c), 2010 Dario Freddi"

#~ msgid ""
#~ "From this module, you can configure the main Power Management daemon, "
#~ "assign profiles to states, and do some advanced fine tuning on battery "
#~ "handling"
#~ msgstr ""
#~ "Από αυτό το άρθρωμα, μπορείτε να ρυθμίσετε τον κύριο δαίμονα διαχείρισης "
#~ "ενέργειας, να αναθέσετε προφίλ στις καταστάσεις, και να κάνετε κάποια "
#~ "προχωρημένη λεπτομερή ρύθμιση σχετικά με το χειρισμό της μπαταρίας."

#~ msgid "Dario Freddi"
#~ msgstr "Dario Freddi"

#~ msgid "Maintainer"
#~ msgstr "Συντηρητής"
