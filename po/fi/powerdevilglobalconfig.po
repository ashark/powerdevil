# Finnish messages for powerdevilglobalconfig.
# Copyright © 2010, 2011 This_file_is_part_of_KDE
# This file is distributed under the same license as the kde-workspace package.
# Jorma Karvonen <karvonen.jorma@gmail.com>, 2010-2011.
# Lasse Liehu <lasse.liehu@gmail.com>, 2011, 2012, 2013, 2014, 2015, 2016.
# Tommi Nieminen <translator@legisign.org>, 2012, 2017, 2019, 2020, 2021.
#
# KDE Finnish translation sprint participants:
msgid ""
msgstr ""
"Project-Id-Version: powerdevilglobalconfig\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-28 02:37+0000\n"
"PO-Revision-Date: 2021-06-07 16:40+0300\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-POT-Import-Date: 2012-12-01 22:22:38+0000\n"
"X-Generator: Lokalize 20.04.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Jorma Karvonen, Lasse Liehu, Tommi Nieminen"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""
"karvonen.jorma@gmail.com, lasse.liehu@gmail.com, translator@legisign.org"

#: GeneralPage.cpp:240
#, kde-format
msgid "The Power Management Service appears not to be running."
msgstr "Virranhallintapalvelu ei vaikuta olevan käynnissä."

#. i18n: ectx: property (text), widget (QLabel, batteryLevelsLabel)
#: generalPage.ui:22
#, kde-format
msgid "<b>Battery Levels                     </b>"
msgstr "<b>Varaustasot</b>"

#. i18n: ectx: property (text), widget (QLabel, lowLabel)
#: generalPage.ui:29
#, kde-format
msgid "&Low level:"
msgstr "Alhainen &varaustaso:"

#. i18n: ectx: property (toolTip), widget (QSpinBox, lowSpin)
#: generalPage.ui:39
#, kde-format
msgid "Low battery level"
msgstr "Alhainen varaustaso"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, lowSpin)
#: generalPage.ui:42
#, kde-format
msgid "Battery will be considered low when it reaches this level"
msgstr "Varaus on alhainen, kun se laskee tälle tasolle"

#. i18n: ectx: property (suffix), widget (QSpinBox, lowSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, criticalSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, lowPeripheralSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, chargeStartThresholdSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, chargeStopThresholdSpin)
#: generalPage.ui:45 generalPage.ui:71 generalPage.ui:114 generalPage.ui:167
#: generalPage.ui:230
#, no-c-format, kde-format
msgid "%"
msgstr " %"

#. i18n: ectx: property (text), widget (QLabel, criticalLabel)
#: generalPage.ui:55
#, kde-format
msgid "&Critical level:"
msgstr "K&riittinen varaustaso:"

#. i18n: ectx: property (toolTip), widget (QSpinBox, criticalSpin)
#: generalPage.ui:65
#, kde-format
msgid "Critical battery level"
msgstr "Kriittinen varaustaso"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, criticalSpin)
#: generalPage.ui:68
#, kde-format
msgid "Battery will be considered critical when it reaches this level"
msgstr "Varaus on kriittinen, kun se laskee tälle tasolle"

#. i18n: ectx: property (text), widget (QLabel, BatteryCriticalLabel)
#: generalPage.ui:81
#, kde-format
msgid "A&t critical level:"
msgstr "K&un akku on kriittisen vähissä:"

#. i18n: ectx: property (text), widget (QLabel, lowPeripheralLabel)
#: generalPage.ui:107
#, kde-format
msgid "Low level for peripheral devices:"
msgstr "Alhainen varaustaso oheislaitteille:"

#. i18n: ectx: property (text), widget (QLabel, batteryThresholdLabel)
#: generalPage.ui:130
#, kde-format
msgid "Charge Limit"
msgstr "Latausrajoitus"

#. i18n: ectx: property (text), widget (QLabel, batteryThresholdExplanation)
#: generalPage.ui:137
#, no-c-format, kde-format
msgid ""
"<html><head/><body><p>Keeping the battery charged 100% over a prolonged "
"period of time may accelerate deterioration of battery health. By limiting "
"the maximum battery charge you can help extend the battery lifespan.</p></"
"body></html>"
msgstr ""
"<html><head/><body><p>Akun latauksen pitäminen täytenä pitkiä aikoja voi "
"kiihdyttää akun kunnon heikkenemistä. Rajoittamalla enimmäislatausta akun "
"käyttöikää saattaa pidentää.</p></body></html>"

#. i18n: ectx: property (text), widget (KMessageWidget, chargeStopThresholdMessage)
#: generalPage.ui:147
#, kde-format
msgid ""
"You might have to disconnect and re-connect the power source to start "
"charging the battery again."
msgstr ""
"Aloittaaksesi akun latauksen uudelleen virtajohto on ehkä irrotettava ja "
"kytkettävä uudelleen."

#. i18n: ectx: property (text), widget (QLabel, chargeStartThresholdLabel)
#: generalPage.ui:157
#, kde-format
msgid "Start charging only once below:"
msgstr "Aloita lataus vasta, kun lataus on alle:"

#. i18n: ectx: property (specialValueText), widget (QSpinBox, chargeStartThresholdSpin)
#: generalPage.ui:164
#, kde-format
msgid "Always charge when plugged in"
msgstr "Lataa aina verkkovirtaan kytkettynä"

#. i18n: ectx: property (text), widget (QLabel, pausePlayersLabel)
#: generalPage.ui:177
#, kde-format
msgid "Pause media players when suspending:"
msgstr "Keskeytä mediasoittimet järjestelmää keskeytettäessä:"

#. i18n: ectx: property (text), widget (QCheckBox, pausePlayersCheckBox)
#: generalPage.ui:184
#, kde-format
msgid "Enabled"
msgstr "Käytössä"

#. i18n: ectx: property (text), widget (QPushButton, notificationsButton)
#: generalPage.ui:203
#, kde-format
msgid "Configure Notifications…"
msgstr "Ilmoitusasetukset…"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: generalPage.ui:216
#, kde-format
msgid "Other Settings"
msgstr "Muut asetukset"

#. i18n: ectx: property (text), widget (QLabel, chargeStopThresholdLabel)
#: generalPage.ui:223
#, kde-format
msgid "Stop charging at:"
msgstr "Lopeta lataus kohdassa:"

#~ msgid "Do nothing"
#~ msgstr "Älä tee mitään"

#~ msgctxt "Suspend to RAM"
#~ msgid "Sleep"
#~ msgstr "Siirry valmiustilaan"

#~ msgid "Hibernate"
#~ msgstr "Siirry lepotilaan"

#~ msgid "Shut down"
#~ msgstr "Sammuta"

# Hieman vapaampi suomennos, mutta välttää kehittäjän turhan ”Tämä voidaan ratkaista” -alun.
#~ msgid ""
#~ "The Power Management Service appears not to be running.\n"
#~ "This can be solved by starting or scheduling it inside \"Startup and "
#~ "Shutdown\""
#~ msgstr ""
#~ "Virranhallintapalvelu ei ole käynnissä.\n"
#~ "Käynnistä tai ajasta se järjestelmäasetusten ”Käynnistys ja sammutus” -"
#~ "osiosta."

#~ msgid "Suspend"
#~ msgstr "Siirry valmiustilaan"

#~ msgid "<b>Events</b>"
#~ msgstr "<b>Tapahtumat</b>"

#~ msgid ""
#~ "When this option is selected, applications will not be allowed to inhibit "
#~ "sleep when the lid is closed"
#~ msgstr ""
#~ "Tällä asetuksella ohjelmien ei sallita estää lepotilaan siirtymistä "
#~ "kantta suljettaessa."

#~ msgid "Never prevent an action on lid close"
#~ msgstr "Älä estä toimintoa kantta suljettaessa"

#~ msgid "Locks screen when waking up from suspension"
#~ msgstr "Lukitse näyttö kun herätään keskeytystilasta"

#~ msgid "You will be asked for a password when resuming from sleep state"
#~ msgstr "Salasanaa kysytään herättäessä lepotilasta"

#~ msgid "Loc&k screen on resume"
#~ msgstr "&Lukitse näyttö herättäessä"

#~ msgid "Battery is at low level at"
#~ msgstr "Akku on vähissä"

#~ msgid "When battery is at critical level"
#~ msgstr "Kun akku on kriittisen vähissä"
