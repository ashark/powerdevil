# translation of powerdevilglobalconfig.po to Latvian
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Maris Nartiss <maris.kde@gmail.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: powerdevilglobalconfig\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-28 02:37+0000\n"
"PO-Revision-Date: 2011-08-24 09:48+0300\n"
"Last-Translator: Maris Nartiss <maris.kde@gmail.com>\n"
"Language-Team: Latvian\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : "
"2);\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Māris Nartišs"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "maris.kde@gmail.com"

#: GeneralPage.cpp:240
#, kde-format
msgid "The Power Management Service appears not to be running."
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, batteryLevelsLabel)
#: generalPage.ui:22
#, fuzzy, kde-format
#| msgid "Battery Levels"
msgid "<b>Battery Levels                     </b>"
msgstr "Baterijas līmeņi"

#. i18n: ectx: property (text), widget (QLabel, lowLabel)
#: generalPage.ui:29
#, fuzzy, kde-format
#| msgid "Low battery level"
msgid "&Low level:"
msgstr "Zems baterijas izlādes līmenis"

#. i18n: ectx: property (toolTip), widget (QSpinBox, lowSpin)
#: generalPage.ui:39
#, kde-format
msgid "Low battery level"
msgstr "Zems baterijas izlādes līmenis"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, lowSpin)
#: generalPage.ui:42
#, kde-format
msgid "Battery will be considered low when it reaches this level"
msgstr ""
"Tiks uzskatīts, ka baterija ir sasniegusi zemu līmeni, kad tās izlādes "
"līmenis būs sasniedzis šo"

#. i18n: ectx: property (suffix), widget (QSpinBox, lowSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, criticalSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, lowPeripheralSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, chargeStartThresholdSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, chargeStopThresholdSpin)
#: generalPage.ui:45 generalPage.ui:71 generalPage.ui:114 generalPage.ui:167
#: generalPage.ui:230
#, no-c-format, kde-format
msgid "%"
msgstr "%"

#. i18n: ectx: property (text), widget (QLabel, criticalLabel)
#: generalPage.ui:55
#, fuzzy, kde-format
#| msgid "Critical battery level"
msgid "&Critical level:"
msgstr "Kritisks baterijas izlādes līmenis"

#. i18n: ectx: property (toolTip), widget (QSpinBox, criticalSpin)
#: generalPage.ui:65
#, kde-format
msgid "Critical battery level"
msgstr "Kritisks baterijas izlādes līmenis"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, criticalSpin)
#: generalPage.ui:68
#, kde-format
msgid "Battery will be considered critical when it reaches this level"
msgstr ""
"Tiks uzskatīts, ka baterija ir sasniegusi kritisku līmeni, kad tās izlādes "
"līmenis būs sasniedzis šo"

#. i18n: ectx: property (text), widget (QLabel, BatteryCriticalLabel)
#: generalPage.ui:81
#, fuzzy, kde-format
#| msgid "Battery is at critical level at"
msgid "A&t critical level:"
msgstr "Baterijas izlāde ir sasniegusi kritisku līmeni"

#. i18n: ectx: property (text), widget (QLabel, lowPeripheralLabel)
#: generalPage.ui:107
#, kde-format
msgid "Low level for peripheral devices:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, batteryThresholdLabel)
#: generalPage.ui:130
#, kde-format
msgid "Charge Limit"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, batteryThresholdExplanation)
#: generalPage.ui:137
#, no-c-format, kde-format
msgid ""
"<html><head/><body><p>Keeping the battery charged 100% over a prolonged "
"period of time may accelerate deterioration of battery health. By limiting "
"the maximum battery charge you can help extend the battery lifespan.</p></"
"body></html>"
msgstr ""

#. i18n: ectx: property (text), widget (KMessageWidget, chargeStopThresholdMessage)
#: generalPage.ui:147
#, kde-format
msgid ""
"You might have to disconnect and re-connect the power source to start "
"charging the battery again."
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, chargeStartThresholdLabel)
#: generalPage.ui:157
#, kde-format
msgid "Start charging only once below:"
msgstr ""

#. i18n: ectx: property (specialValueText), widget (QSpinBox, chargeStartThresholdSpin)
#: generalPage.ui:164
#, kde-format
msgid "Always charge when plugged in"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, pausePlayersLabel)
#: generalPage.ui:177
#, kde-format
msgid "Pause media players when suspending:"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, pausePlayersCheckBox)
#: generalPage.ui:184
#, kde-format
msgid "Enabled"
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, notificationsButton)
#: generalPage.ui:203
#, fuzzy, kde-format
#| msgid "Configure Notifications..."
msgid "Configure Notifications…"
msgstr "Konfigurēt ziņojumus..."

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: generalPage.ui:216
#, fuzzy, kde-format
#| msgid "Advanced Battery Settings"
msgid "Other Settings"
msgstr "Paplašināti baterijas iestatījumi"

#. i18n: ectx: property (text), widget (QLabel, chargeStopThresholdLabel)
#: generalPage.ui:223
#, kde-format
msgid "Stop charging at:"
msgstr ""

#~ msgid "Do nothing"
#~ msgstr "Nedarīt neko"

#, fuzzy
#~| msgid "Sleep"
#~ msgctxt "Suspend to RAM"
#~ msgid "Sleep"
#~ msgstr "Iesnaudināt"

#~ msgid "Hibernate"
#~ msgstr "Hibrenēt"

#, fuzzy
#~| msgid "Shutdown"
#~ msgid "Shut down"
#~ msgstr "Izslēgt"

#~ msgid "<b>Events</b>"
#~ msgstr "<b>Notikumi</b>"

#~ msgid "Locks screen when waking up from suspension"
#~ msgstr "Pamostoties no iesnaudināšanas slēdz ekrānu"

#~ msgid "You will be asked for a password when resuming from sleep state"
#~ msgstr ""
#~ "Jums tiks prasīts ievadīt paroli, kad dators mostas no iesnaudināšanas"

#, fuzzy
#~| msgid "Lock screen on resume"
#~ msgid "Loc&k screen on resume"
#~ msgstr "Atsākot slēgt ekrānu"

#~ msgid "Battery is at low level at"
#~ msgstr "Baterijas izlāde ir sasniegusi zemu līmeni"

#, fuzzy
#~| msgid "When battery is at low level"
#~ msgid "When battery is at critical level"
#~ msgstr "Kad baterija ir sasniegusi zemu līmeni"

#~ msgid "Global Power Management Configuration"
#~ msgstr "Globālā energokontroles konfigurācija"

#~ msgid ""
#~ "A global power management configurator for KDE Power Management System"
#~ msgstr ""
#~ "Globāla energokontroles konfigurācija priekš KDE energokontroles sistēmas"

#~ msgid "(c), 2010 Dario Freddi"
#~ msgstr "(c), 2010 Dario Freddi"

#~ msgid ""
#~ "From this module, you can configure the main Power Management daemon, "
#~ "assign profiles to states, and do some advanced fine tuning on battery "
#~ "handling"
#~ msgstr ""
#~ "Šajā modulī ir iespējams konfigurēt galveno energokontroles pārvaldes "
#~ "dēmonu, piešķirt profiliem stāvokļus un pieregulēt paplašinātus baterijas "
#~ "pārvaldības parametrus"

#~ msgid "Dario Freddi"
#~ msgstr "Dario Freddi"

#~ msgid "Maintainer"
#~ msgstr "Uzturētājs"

#~ msgid "Settings and Profile"
#~ msgstr "Iestatījumi un profils"

#~ msgid ""
#~ "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
#~ "REC-html40/strict.dtd\">\n"
#~ "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/"
#~ "css\">\n"
#~ "p, li { white-space: pre-wrap; }\n"
#~ "</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; "
#~ "font-weight:400; font-style:normal;\">\n"
#~ "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
#~ "right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
#~ "weight:600;\">Profile Assignment</span></p></body></html>"
#~ msgstr ""
#~ "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
#~ "REC-html40/strict.dtd\">\n"
#~ "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/"
#~ "css\">\n"
#~ "p, li { white-space: pre-wrap; }\n"
#~ "</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; "
#~ "font-weight:400; font-style:normal;\">\n"
#~ "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
#~ "right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
#~ "weight:600;\">Profilu piešķiršana</span></p></body></html>"

#~ msgid "When AC Adaptor is plugged in"
#~ msgstr "Kad ir iesprausts vads"

#~ msgid "When AC Adaptor is unplugged"
#~ msgstr "Kad vads ir izrauts"

#~ msgid "When battery is at warning level"
#~ msgstr "Kad baterija ir sasniegusi brīdinājuma līmeni"

#~ msgid "When battery remaining is critical"
#~ msgstr "Kad atlikušais baterijas izlādes līmenis ir kritisks"

#~ msgid "Battery is at warning level at"
#~ msgstr "Baterijas izlāde ir sasniegusi brīdinājuma līmeni"

#~ msgid "Warning battery level"
#~ msgstr "Brīdinājuma baterijas izlādes līmenis"

#~ msgid ""
#~ "Battery will be considered at warning level when it reaches this level"
#~ msgstr ""
#~ "Tiks uzskatīts, ka baterija ir sasniegusi brīdinājuma līmeni, kad tās "
#~ "izlādes līmenis būs sasniedzis šo"
